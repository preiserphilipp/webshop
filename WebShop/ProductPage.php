<?php
	include 'bootstrapHeader.php';
	include 'dbAccess.php'; 
	include 'navbar.php';
?>

		
			<?php 
				$res = $db->query("SELECT artikel.ID as artID, artikel.Name, artikel.Bild as Bild, kategorie.Name as katName, artikel.Preis, artikel.Lagerstandmenge, artikel.Beschreibung, lieferanten.Vorname, lieferanten.Nachname, lieferanten.Organisation
									FROM artikel 
									LEFT JOIN kategorie
									ON artikel.KategorieID = kategorie.ID
									LEFT JOIN lieferanten
									ON artikel.Lieferanten_ID = lieferanten.ID
									WHERE artikel.ID = '".$_GET['artikelID']."'");
				$tmp = $res->fetchAll(PDO::FETCH_ASSOC);
				
				foreach($tmp as $row)
				{
				$kat = $row['katName'];
			?>	
	 			<div class="row clearfix">
					<div class="col-md-2 column">
						<img alt="140x140" height="150px" width="150px" src="product-imgs/<?php echo $row['Bild']; ?>">
					</div>
					
					<div class="col-md-6 column">
						<dl>
							<dt>
								Name
							</dt>
							<dd>
								<?php echo $row['Name']; ?>
							</dd>
							<dt>
								Preis
							</dt>
							<dd>
								<?php echo $row['Preis']; ?> &euro;
							</dd>
							<dt>
								Kategorie
							</dt>
							<dd>
								<a href="Listpage.php?kategorie=<?php echo $kat; ?>"><?php echo $kat; ?></a>
							</dd>
						</dl>
					</div>
					
					<div class="col-md-4 column">
						<a href=<?php echo "addToCart.php?artikelID=".$row['artID'] ?>>
							<input type="button" name="Submit" class="btn btn-info" value="In den Warenkorb" />
						</a>
						<dl class="dl-horizontal">
							<dt>
								Lagerstand:
							</dt>
							<dd>
								<?php echo $row['Lagerstandmenge']; ?>
							</dd>
						</dl>
					</div>
				</div>
				<dl>
					<dt>
						Description
					</dt>
					<dd>
						<?php echo $row['Beschreibung']; ?>
					</dd>
				</dl>
 					
 					
			<?php 
				}
				echo "</div>";
				include 'newest.php';
			?>			
			
		</div>
	</div>
</div>
</body>
</html>
