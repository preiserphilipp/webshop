<?php
	require 'class.phpmailer.php';
	
	$mail = new PHPMailer;
	
	$mail->isSMTP();										// Set mailer to use SMTP
	$mail->Host = 'smtp.gmail.com';							// Specify main and backup server
	$mail->SMTPAuth = true;									// Enable SMTP authentication
	
	$mail->Username = 'htlzwettl.itp.project@gmail.com';	// SMTP username
	$mail->Password = 'htlzwettl';							// SMTP password
	$mail->SMTPSecure = 'ssl';								// Enable encryption, 'ssl' also accepted
	$mail->Port = 465;
	
	//$mail->WordWrap = 70;									// Set word wrap to 50 characters
	$mail->isHTML(true);									// Set email format to HTML
	
	$mail->AltBody = 'Achtung! \nBitte verwenden Sie ein E-Mail-Programm, welches die Darstellung von HTML-Elementen unterstützt!';
	
	$mail->FromName = $fromName;
	$mail->addAddress($addAddress);		// Add a recipient
	$mail->addReplyTo('htlzwettl.itp.project@gmail.com', 'WebShop - KundenSupport');
	
	$mail->Subject = $subject;
	$mail->Body    = $body;
	
	
	if(!$mail->send()) {
		echo 'Message could not be sent.</br>';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
		exit;
	}
	echo 'Message has been sent';
?>