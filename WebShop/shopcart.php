<?php
	include 'bootstrapHeader.php';
	include 'dbAccess.php'; 
	include 'navbar.php'; 
	$gesamtpreis=0;
	$summecart=0;
	
	$res = $db->query("SELECT artikel.ID as ID, artikel.Name as name, artikel.Preis as preis, warenkorb.menge as menge
					   FROM `warenkorb` 
					   left join `artikel` 
					   on warenkorb.artikel_ID = artikel.ID 
					   WHERE kunden_ID = ".$_SESSION['ID']);
	$tmp = $res->fetchAll(PDO::FETCH_ASSOC);
	
?>
	<script type="text/javascript" src="js/scripts.js">
	</script>
	<script>
		function deleteProject(pid){ 
				if(confirm("Entferne Item "+ pid + " aus dem Warenkorb?"))
				{
					console.log("YES: " + pid);

					$.ajax({ // $.ajax = Kurzschreibweise von jQuery.ajax
						data:"deletePro=" + pid,
						type: "POST",
						url: "ajaxRequestHandler.php",
						success: function(response, textStatus, jqXHR){ //Callback-Funktionen
							console.log("Antwort: " + response);
							// 1. Text in "infoBox" setzen
							// 2. Hintergrund setzen
							// 3. Zeile löschen
							$('#infobox').addClass('successInfo');
							$('#infobox').html('Löschen erfolgreich');
							$('#row_'+pid).remove();
							
						},
						error: function(jqXHR, textStatus, errorThrown){
							console.log("Error: " + textStatus);
							// 1. Error-Class setzen
							// 2. Error-Info
							$('#infobox').addClass('errorInfo');
							$('#infobox').html('Löschen fehlgeschlagen!');
						}
					});
				}
				else
				{
					console.log("NO");
				}
			}
	</script>
	<style>

      table { 
      		border: 1px solid #555;
      		border-collapse: collapse;
      		}
      th{
      	background-color: grey;
      }
      tbody td{
      	background-color: lightgrey;
      }
      tfoot td{
      	background-color: white;
      }

    </style>
</head>

<body>
<div class="container">

	<div class="row clearfix">
		<div class="col-md-6 column">
			<h1>
				Ihr Warenkorb
			</h1>
			<form action="shopcart.php" method="post" enctype="multipart/formdata">
				<table summary="Summe der Artikel im Warenkorb" style="table-layout:fixed">
				  <tr>
				    <th scope="col" width="8%">Anzahl</th>
				    <th scope="col" width="50%">Produkt</th>
				    <th scope="col" width="18%">Einzelpreis</th>
				    <th scope="col" width="18%">Gesamt</th>
				    <th scope="col" width="6%"> </th>
				  </tr>
				</thead>

				<tbody>
					<?php
					foreach($tmp as $row)
					{
						$gesamtpreis = $row['preis']*$row['menge'];
					  	echo "<tr>";
				    	echo"<td><input type='text' size='1' value='".$row['menge']."'></td>";
				    	echo"<td><a href=ProductPage.php?artikelID=".$row['ID'].">".$row['name']."</a></td>";
				    	echo"<td>".$row['preis']."€</td>";
				    	echo"<td>".$gesamtpreis."€</td>";
    					echo"<td><a style='cursor:pointer' onclick='deleteProject(".$row['ID'].")'><img src='./img/b_drop.png'></td>";
				  		echo"</tr>";
				  		$summecart = $summecart + $gesamtpreis;

					}
				  ?>
				</tbody>

				<tfoot>
				  <tr>
				    <td colspan="3">Summe Warenkorb:</td>
				    <td><?php echo "$summecart"; ?>€</td>
				    <td> </td>
				  </tr>
				</tfoot>
				</table >
				<!--<input type="Submit" name="Submit" class="btn btn-info" value="Bestellung abschicken">-->
					<a href="ajaxRequestHandlerOrder.php">Bestellen</a>
				<!--</input>-->
			</form>
		</div>
		<div class="row clearfix">
			<div class="col-md-6 column">
			<?php include 'newest.php'; ?>
		</div>
		</div>
	</div>
</div>
</body>
</html>