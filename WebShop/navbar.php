<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h3>
				Amaklon
			</h3>
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only"> Toggle navigation </span>
						 <span class="icon-bar"></span>
						 <span class="icon-bar"></span>
						 <span class="icon-bar"></span>
					 </button>
					 <a class="navbar-brand" href="MainPage.php">( ͡° ͜ʖ ͡°)</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Kategorie <strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								
									<?php

									$res = $db->query("Select * from kategorie");
									$tmp = $res->fetchAll(PDO::FETCH_ASSOC);
									foreach ($tmp as $key)
									{

										echo "<li><a href='ListPage.php?kategorie=".$key['Name']."'>".$key['Name']."</a></li>";
									} 
									?>
							</ul>
						</li>
						<li>
						<?php
							echo "<a href='ListPage.php?kategorie=Neueste'>Neueste</a>";
							?>
						</li>
						<li>
							<form class="navbar-form navbar-left" role="search" action="ListPage.php">
							<div class="form-group">
							<input type="text" class="form-control" name="search"/>
							</div> 
							<button type='submit' class='btn btn-default'>Suchen</button>
							
							</form>
						</li>
							<?php 
								if(isset($_SESSION['UserVorname']))
								{
									echo "<li class='dropdown'>
												<a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$_SESSION['UserVorname']."<strong class='caret'></strong></a>
												<ul class='dropdown-menu'>
													<li><a href='logout.php'>Logout</a></li>
													<li><a href='shopcart.php'>Warenkorb</a></li>
												</ul>
										</li>"
										;
								}
								else
									echo "<li>
											<a href='login.php'>Log In</a>
										</li>
										<li>
											<a href='register.php'>Registrieren</a>
										</li>
										";
							?>
						
					</ul>
					<?php //<form class="navbar-form navbar-left" role="search">
						  //<div class="form-group">
						  //	<input type="text" class="form-control">
						  //</div> <button type="submit" class="btn btn-default">Submit</button>
						  //</form>
					?>
					
				</div>
				
			</nav>