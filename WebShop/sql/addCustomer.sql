use Webshop;
Drop Procedure if exists addCustomer;
DELIMITER //
CREATE PROCEDURE addCustomer(IN pwt varchar(45))
BEGIN
	DECLARE pid int(11) DEFAULT 0;
	SELECT ID
	INTO pid
	from person
	where ID = LAST_INSERT_ID();

	INSERT INTO kunden(PersonID, Passwort) VALUES (pid, pwt);
END //
DELIMITER ;	