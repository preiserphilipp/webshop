use Webshop;
Drop Procedure if exists addUser;
DELIMITER //
CREATE PROCEDURE addUSER(IN email varchar(45), IN vname varchar(45), IN nname varchar(45), IN tel varchar(45), IN adresse varchar(45))
BEGIN
	INSERT INTO person(Vorname,Nachname, Telefonnummer, Adresse,`E-Mail-Adresse`) VALUES (vname, nname,tel,adresse,email);
END //
DELIMITER ;	