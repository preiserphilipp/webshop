-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 20. Jan 2015 um 16:29
-- Server Version: 5.6.20
-- PHP-Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `webshop`
--

DELIMITER $$
--
-- Prozeduren
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `addCustomer`(IN pwt varchar(45))
BEGIN
	DECLARE pid int(11) DEFAULT 0;
	
	SELECT ID
	INTO pid
	from person
	where ID = LAST_INSERT_ID();
	
	INSERT INTO kunden(PersonID, Passwort) VALUES (pid, pwt);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `addUSER`(IN email varchar(45), IN vname varchar(45), IN nname varchar(45), IN tel varchar(45), IN adresse varchar(45))
BEGIN
	INSERT INTO person(Vorname,Nachname, Telefonnummer, Adresse,`E-Mail-Adresse`) VALUES (vname, nname,tel,adresse,email);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `returnNewest`()
BEGIN
	Select MAX(ID) FROM artikel;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ansprechperson`
--

CREATE TABLE IF NOT EXISTS `ansprechperson` (
`ID` int(11) NOT NULL,
  `Lieferanten_ID` int(11) NOT NULL,
  `Abteilung` varchar(45) DEFAULT NULL,
  `Person_ID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `ansprechperson`
--

INSERT INTO `ansprechperson` (`ID`, `Lieferanten_ID`, `Abteilung`, `Person_ID`) VALUES
(1, 2, 'Accounting', 1),
(2, 1, 'Sales', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `artikel`
--

CREATE TABLE IF NOT EXISTS `artikel` (
`ID` int(11) NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Bild` varchar(45) DEFAULT NULL,
  `KategorieID` int(11) DEFAULT NULL,
  `Preis` double DEFAULT NULL,
  `Lagerstandmenge` int(11) DEFAULT NULL,
  `Beschreibung` varchar(300) DEFAULT NULL,
  `Lieferanten_ID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `artikel`
--

INSERT INTO `artikel` (`ID`, `Name`, `Bild`, `KategorieID`, `Preis`, `Lagerstandmenge`, `Beschreibung`, `Lieferanten_ID`) VALUES
(1, 'asdf', 'Tisch-201020506976.jpg', 3, 1234.1234, 9, 'asdf', 1),
(2, 'sdfg', 'Loss.jpg', 4, 2345.2345, 23, 'sdfg', 2),
(3, 'Folterbank', 'folterbank.jpg', 3, 1269.99, 2, 'Fesselnde Antiquität', 1),
(4, 'Fibbity', 'fibbidy.jpg', 4, 99.33, 5, 'Leider aus', 2),
(5, 'Schwert', 'claymore.jpg', 4, 699.99, 2, 'Messer', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kategorie`
--

CREATE TABLE IF NOT EXISTS `kategorie` (
`ID` int(11) NOT NULL,
  `Name` varchar(60) NOT NULL,
  `Bild` varchar(200) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `kategorie`
--

INSERT INTO `kategorie` (`ID`, `Name`, `Bild`) VALUES
(1, 'Eimer', 'eimer.png'),
(3, 'Tisch', 'tisch.jpg'),
(4, 'Geschirr', 'geschirr.jpg');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kunden`
--

CREATE TABLE IF NOT EXISTS `kunden` (
`ID` int(11) NOT NULL,
  `PersonID` int(11) NOT NULL,
  `Passwort` varchar(45) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `kunden`
--

INSERT INTO `kunden` (`ID`, `PersonID`, `Passwort`) VALUES
(1, 1, 'pwd'),
(2, 2, 'bla'),
(3, 15, '1234');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `käufe`
--

CREATE TABLE IF NOT EXISTS `käufe` (
`ID` int(11) NOT NULL,
  `Kunden_ID` int(11) NOT NULL,
  `Versandart` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `käufe_has_artikel`
--

CREATE TABLE IF NOT EXISTS `käufe_has_artikel` (
`Käufe_ID` int(11) NOT NULL,
  `Artikel_ID` int(11) NOT NULL,
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lieferanten`
--

CREATE TABLE IF NOT EXISTS `lieferanten` (
`ID` int(11) NOT NULL,
  `Vorname` varchar(45) DEFAULT NULL,
  `Nachname` varchar(45) DEFAULT NULL,
  `Organisation` varchar(45) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `lieferanten`
--

INSERT INTO `lieferanten` (`ID`, `Vorname`, `Nachname`, `Organisation`) VALUES
(1, 'Josef', 'Müller', 'UPS'),
(2, 'Adrian', 'Kern', 'Post AG');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `person`
--

CREATE TABLE IF NOT EXISTS `person` (
`ID` int(11) NOT NULL,
  `Vorname` varchar(45) DEFAULT NULL,
  `Nachname` varchar(45) DEFAULT NULL,
  `Telefonnummer` varchar(45) DEFAULT NULL,
  `Adresse` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Daten für Tabelle `person`
--

INSERT INTO `person` (`ID`, `Vorname`, `Nachname`, `Telefonnummer`, `Adresse`, `Email`) VALUES
(1, 'Philipp', 'Preiser', '0660', 'Pletzensiedlung', 'preiser.philipp@gmail.com'),
(2, 'Julia', 'Honeder', '0664', 'Kirchschlag', 'honeder.j@gmail.com'),
(13, 'J', 'H', '123', 'K', 'hilfe@hilfe'),
(14, 'p', 'p', '1', 'p', 'p'),
(15, 'Christian', 'Prager', '11223344', 'Franz-Human Gasse 210', 'C.prager@gmx.at');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rücksendungen`
--

CREATE TABLE IF NOT EXISTS `rücksendungen` (
`ID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `rücksendungen`
--

INSERT INTO `rücksendungen` (`ID`) VALUES
(1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rücksendungen_has_artikel_&_käufe`
--

CREATE TABLE IF NOT EXISTS `rücksendungen_has_artikel_&_käufe` (
  `Rücksendungen_ID` int(11) NOT NULL,
  `Artikel_ID` int(11) NOT NULL,
  `Käufe_ID` int(11) NOT NULL,
`ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `versandart`
--

CREATE TABLE IF NOT EXISTS `versandart` (
`id` int(11) NOT NULL,
  `Versandbeschreibung` varchar(45) DEFAULT NULL,
  `Versandkosten` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `versandart`
--

INSERT INTO `versandart` (`id`, `Versandbeschreibung`, `Versandkosten`) VALUES
(1, 'Flugzeug', 14),
(2, 'Postauto', 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `warenkorb`
--

CREATE TABLE IF NOT EXISTS `warenkorb` (
  `kunden_ID` int(11) NOT NULL,
  `artikel_ID` int(11) NOT NULL,
  `Menge` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ansprechperson`
--
ALTER TABLE `ansprechperson`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_Ansprechperson_Lieferanten1_idx` (`Lieferanten_ID`), ADD KEY `fk_Ansprechperson_Person1_idx` (`Person_ID`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_Artikel_Lieferanten1_idx` (`Lieferanten_ID`), ADD KEY `KategorieID` (`KategorieID`);

--
-- Indexes for table `kategorie`
--
ALTER TABLE `kategorie`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `kunden`
--
ALTER TABLE `kunden`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `käufe`
--
ALTER TABLE `käufe`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_Käufe_Kunden1_idx` (`Kunden_ID`), ADD KEY `Versandart` (`Versandart`);

--
-- Indexes for table `käufe_has_artikel`
--
ALTER TABLE `käufe_has_artikel`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_Käufe_has_Artikel_Artikel1_idx` (`Artikel_ID`), ADD KEY `fk_Käufe_has_Artikel_Käufe1_idx` (`Käufe_ID`);

--
-- Indexes for table `lieferanten`
--
ALTER TABLE `lieferanten`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rücksendungen`
--
ALTER TABLE `rücksendungen`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rücksendungen_has_artikel_&_käufe`
--
ALTER TABLE `rücksendungen_has_artikel_&_käufe`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_Rücksendungen_has_Artikel_Artikel1_idx` (`Artikel_ID`), ADD KEY `fk_Rücksendungen_has_Artikel_Rücksendungen1_idx` (`Rücksendungen_ID`), ADD KEY `fk_Rücksendungen_has_Artikel_Käufe1_idx` (`Käufe_ID`);

--
-- Indexes for table `versandart`
--
ALTER TABLE `versandart`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warenkorb`
--
ALTER TABLE `warenkorb`
 ADD PRIMARY KEY (`kunden_ID`,`artikel_ID`), ADD KEY `fk_kunden_has_artikel_artikel1_idx` (`artikel_ID`), ADD KEY `fk_kunden_has_artikel_kunden1_idx` (`kunden_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ansprechperson`
--
ALTER TABLE `ansprechperson`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kategorie`
--
ALTER TABLE `kategorie`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kunden`
--
ALTER TABLE `kunden`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `käufe`
--
ALTER TABLE `käufe`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `käufe_has_artikel`
--
ALTER TABLE `käufe_has_artikel`
MODIFY `Käufe_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lieferanten`
--
ALTER TABLE `lieferanten`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `rücksendungen`
--
ALTER TABLE `rücksendungen`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `rücksendungen_has_artikel_&_käufe`
--
ALTER TABLE `rücksendungen_has_artikel_&_käufe`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `versandart`
--
ALTER TABLE `versandart`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `ansprechperson`
--
ALTER TABLE `ansprechperson`
ADD CONSTRAINT `fk_Ansprechperson_Lieferanten1` FOREIGN KEY (`Lieferanten_ID`) REFERENCES `lieferanten` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Ansprechperson_Person1` FOREIGN KEY (`Person_ID`) REFERENCES `person` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `artikel`
--
ALTER TABLE `artikel`
ADD CONSTRAINT `artikel_ibfk_1` FOREIGN KEY (`KategorieID`) REFERENCES `kategorie` (`ID`),
ADD CONSTRAINT `fk_Artikel_Lieferanten1` FOREIGN KEY (`Lieferanten_ID`) REFERENCES `lieferanten` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `käufe`
--
ALTER TABLE `käufe`
ADD CONSTRAINT `fk_Käufe_Kunden1` FOREIGN KEY (`Kunden_ID`) REFERENCES `kunden` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `käufe_ibfk_1` FOREIGN KEY (`Versandart`) REFERENCES `versandart` (`id`);

--
-- Constraints der Tabelle `käufe_has_artikel`
--
ALTER TABLE `käufe_has_artikel`
ADD CONSTRAINT `fk_Käufe_has_Artikel_Artikel1` FOREIGN KEY (`Artikel_ID`) REFERENCES `artikel` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Käufe_has_Artikel_Käufe1` FOREIGN KEY (`Käufe_ID`) REFERENCES `käufe` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `rücksendungen_has_artikel_&_käufe`
--
ALTER TABLE `rücksendungen_has_artikel_&_käufe`
ADD CONSTRAINT `fk_Rücksendungen_has_Artikel_Artikel1` FOREIGN KEY (`Artikel_ID`) REFERENCES `artikel` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Rücksendungen_has_Artikel_Käufe1` FOREIGN KEY (`Käufe_ID`) REFERENCES `käufe` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Rücksendungen_has_Artikel_Rücksendungen1` FOREIGN KEY (`Rücksendungen_ID`) REFERENCES `rücksendungen` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `warenkorb`
--
ALTER TABLE `warenkorb`
ADD CONSTRAINT `fk_kunden_has_artikel_artikel1` FOREIGN KEY (`artikel_ID`) REFERENCES `artikel` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_kunden_has_artikel_kunden1` FOREIGN KEY (`kunden_ID`) REFERENCES `kunden` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
