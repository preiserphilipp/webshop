Select artikel.ID, artikel.Name, artikel.Bild, kategorie.Name, artikel.Preis, artikel.Lagerstandmenge, artikel.Beschreibung, lieferanten.Vorname, lieferanten.Nachname, lieferanten.Organisation
from artikel 
left join kategorie
on artikel.KategorieID = kategorie.ID
left join lieferanten
on artikel.Lieferanten_ID = lieferanten.ID