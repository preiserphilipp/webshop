<?php
	include 'bootstrapHeader.php';
	include 'dbAccess.php'; 
	include 'navbar.php';
?>

			<div class="row clearfix">
				<div class="col-md-6 column">
					<h3>
						<?php 
							if(isset($_GET['kategorie']))
							echo $_GET['kategorie']; 
							else
							echo 'Suchergebnisse für "'.$_GET['search'].'"';
						?>
					</h3>
					
					<?php 
					if(isset($_GET['search']))
					{
						$res = $db->query("SELECT artikel.ID, artikel.Name, artikel.Bild, artikel.Preis, artikel.Lagerstandmenge, kategorie.Name as kategorie
												FROM artikel
												left join kategorie on kategorie.id = artikel.KategorieID
												WHERE artikel.Name like '%".$_GET['search']."%'");
					}
					else if($_GET['kategorie']=='Neueste')
					{
							$res = $db->query("SELECT artikel.ID, artikel.Name, artikel.Bild, artikel.Preis, artikel.Lagerstandmenge, kategorie.Name as kategorie
												FROM artikel
												left join kategorie on kategorie.id = artikel.KategorieID
												ORDER BY ~artikel.ID
												LIMIT 0,4");
					}
					else
					{
							$res = $db->query("SELECT artikel.ID, artikel.Name, artikel.Bild, artikel.Preis, artikel.Lagerstandmenge
												FROM artikel
												Right JOIN kategorie ON kategorie.id = artikel.KategorieID
												WHERE kategorie.name = '".$_GET['kategorie']."'");
					}
					$tmp = $res->fetchAll(PDO::FETCH_ASSOC);
					foreach($tmp as $row)
					{
						if(!(is_null($row['ID'])))
						{
						?>	
	 					
							<div class="row clearfix">
								<div class="col-md-6 column">
									<img alt="140x140" height="150px" width="150px" src="product-imgs/<?php echo $row['Bild'];?>">
								</div>

								<div class="col-md-6 column">
									<dl>
										<dt>
											Name
										</dt>
										<dd>
											<?php echo $row['Name']; ?>
										</dd>
										<dt>
											Preis
										</dt>
										<dd>
											<?php echo $row['Preis']; ?> &euro;
										</dd>
										<?php
										
										if($row['Lagerstandmenge'] == 0)
										{
											echo"<dd style='color:#FF0000'>";
											echo "Ausverkauft!";
											echo"</dd>";
										}
										elseif($row['Lagerstandmenge'] < 3 && $row['Lagerstandmenge'] != 0)
										{
											echo"<dd style='color:#FF0000'>";
											echo "Nur noch ".$row['Lagerstandmenge']." auf Lager!";
											echo"</dd>";
										}
										elseif($row['Lagerstandmenge'] < 5 && $row['Lagerstandmenge'] != 0)
										{
											echo "<dd style='color:#FFA500'>";
											echo "Nur noch ".$row['Lagerstandmenge']." auf Lager";
											echo"</dd>";
										}
										if((isset($_GET['kategorie'])&&$_GET['kategorie']=='Neueste') || isset($_GET['search']))
										{
											echo "<dt>
												Kategorie
											</dt>
											<dd>
												<a href='ListPage.php?kategorie=".$row['kategorie']."'>".$row['kategorie']."</a>
											</dd>";
										}
										?>
										<dt>
											<a href=<?php echo "'ProductPage.php?artikelID=".$row['ID']."'"; ?> >Mehr Infos</a>
										</dt>
										<dt>
											<a href=<?php echo "addToCart.php?artikelID=".$row['ID'] ?>>Zum Warenkorb</a>
										</dt>
									</dl>
								</div>
							</div>
					<?php
						} 
						else
						{
							echo "Keine Einträge gefunden!";
						}
					}
					
						echo "</div>";
						if(!isset($_GET['kategorie']) || $_GET['kategorie']!='Neueste')
						{ include 'newest.php'; }
					?>
					
				</div>
			</div>
			
			
			
		</div>
	</div>
</div>
</body>
</html>
