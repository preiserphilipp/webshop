<?php
	include 'bootstrapHeader.php';
	include 'dbAccess.php'; 
	include 'navbar.php';	
	?>

			<div class="row clearfix">
				<div class="col-md-6 column">
					<h3>
						Kategorien
					</h3>
					
					
					<?php 
						$res = $db->query("SELECT Name, Bild FROM kategorie");
						$tmp = $res->fetchAll(PDO::FETCH_ASSOC);
						
						foreach($tmp as $row)
						{

					?>	
					<p>
						<a href=<?php echo "'ListPage.php?kategorie=".$row['Name']."'"; ?> >
							<div class="row clearfix">
								<div class="col-md-6 column">
								<img alt="140x140" height="150px" width="150px" src="kat-imgs/<?php echo $row['Bild'];?>">
								</div>
								<div class="col-md-6 column">
									<dl>
										<dt>
											<?php echo $row['Name']; ?>
										</dt>
									</dl>
								</div>
							</div>
						</a>
					</p>
					<?php 
					}
					echo "</div>";
					include 'newest.php';
					?>
			</div>
			<div class="row clearfix">
				<div class="col-md-12 column">
					<h3>
						Was Sie interessieren könnte
					</h3>
					<?php 
					$res = $db -> query("SELECT COUNT(ID) from Artikel");
					$newest = $res -> fetchAll(PDO::FETCH_ASSOC);
					
					do{
						foreach($newest as $item)
						{
						$rand = array(rand(1,$item['COUNT(ID)']), rand(1,$item['COUNT(ID)']), rand(1,$item['COUNT(ID)']));
						}
					}while($rand[0]==$rand[1]||$rand[0]==$rand[2]||$rand[1]==$rand[2]);

					$res = $db -> query("SELECT Bild, Name, Preis, ID, Lagerstandmenge
										 FROM Artikel WHERE ID = ".$rand[0]." || ID =".$rand[1]." || ID =".$rand[2]." LIMIT 0,3");
					$newet = $res -> fetchAll(PDO::FETCH_ASSOC);
					
					foreach($newet as $random)
					{
					?>
				<div class="col-md-4 column">
					<img alt="140x140" width="150px" height="150px" src="product-imgs/<?php echo $random['Bild']?>">
					<dl>
						<dt>
							<span>Name</span>
						</dt>
						<dd>
						<a href=<?php echo "'ProductPage.php?artikelID=".$random['ID']."'";?> >
							<?php echo $random['Name'];?>
						</a>
						</dd>
						<dt>
							<span>Preis</span>
						</dt>
						<dd>
						<?php echo $random['Preis'];?>
						</dd>
						<?php
						if($random['Lagerstandmenge'] < 3)
						{
							echo"<dd style='color:#FF0000'>";
							echo "Nur noch ".$random['Lagerstandmenge']." auf Lager!";
							echo"</dd>";
						}
						elseif($random['Lagerstandmenge'] < 5)
						{
							echo"<dd style='color:#FFA500'>";
							echo "Nur noch ".$random['Lagerstandmenge']." auf Lager";
							echo"</dd>";
						}
						?>
					</dl>
				</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
